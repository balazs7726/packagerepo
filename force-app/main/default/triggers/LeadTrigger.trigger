trigger LeadTrigger on Lead(before insert, after insert, before update, after update) {
	System.debug('🏃‍♂️ run QuoteTrigger');

	if (LeadTriggerHandler.bypassTrigger) {
		System.debug('skipped QuoteTrigger');
		return;
	}
	if (Trigger.isBefore) {
		if (Trigger.isInsert) {
			LeadTriggerHandler.handleBeforeInsert(Trigger.new);
		}
		if (Trigger.isUpdate) {
			LeadTriggerHandler.handleBeforeUpdate(Trigger.oldMap, Trigger.newMap);
		}
	}

	if (Trigger.isAfter) {
		if (Trigger.isInsert) {
			LeadTriggerHandler.handleAfterInsert(Trigger.newMap);
		}
		if (Trigger.isUpdate) {
			LeadTriggerHandler.handleAfterUpdate(Trigger.oldMap, Trigger.newMap);
		}
	}
}