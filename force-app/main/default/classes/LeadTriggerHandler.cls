public class LeadTriggerHandler {
	public static Boolean bypassTrigger = false;

	public static void handleBeforeInsert(List<Lead> newList) {

    }

    public static void handleAfterInsert(Map<Id, Lead> newMap) {
        
    }

    public static void handleBeforeUpdate(Map<Id, Lead> oldMap, Map<Id, Lead> newMap) {
       
    }

    public static void handleAfterUpdate(Map<Id, Lead> oldMap, Map<Id, Lead> newMap) {
    	LeadTriggerService.addTime1(newMap.values());
       	LeadTriggerService.addTime2(newMap.values());
    }
}